use std::process::Command;
use tokio::fs;
use std::io::*;
use serde::{Serialize, Deserialize};
use tokio::io::{AsyncWriteExt, AsyncReadExt};
use tokio::net::TcpStream;
use clap::{Arg, App};

#[tokio::main]
async fn main() {

    let cmdln = App::new("Faucet Client")
        .version("0.1100101000")
        .author("Caleb B. calebrobot88@gmail.com")
        .about("Turns your computer into a \"worker")
        .arg(Arg::with_name("address").
             short("i")
             .long("address")
             .value_name("IP_ADDRESS")
             .help("Sets the IP address of the server")
             .takes_value(true))
        .get_matches();

    let addr = cmdln.value_of("address").unwrap();
        //.unwrap_or("localhost");
    let addr = format!("{}:27182", addr);

    let mut stream = TcpStream::connect(&addr).await.expect("Couldn't connect to the server");
    send_stuff(&Role::Slave, &mut stream).await;

    stream.flush().await.unwrap();    
    let mut steve = handle_packet(&mut stream).await;
    stream.shutdown().await.expect("Couldn't Shutdown Stream");
    drop(stream);
    let file_path = write_blend(&mut steve.file).await;
    let _os = if cfg!(target_os = "windows") {
        if let Err{..} = fs::create_dir("renders").await {
        }
        Command::new("blender")
                .args(&["-b", file_path, "-o","//renders/render####",
                 "-F", "PNG", "-x", "1", "-f", &format!("{}..{}", steve.frame_range.0, steve.frame_range.1)])
                .output()
                .unwrap()
    } else {
        if let Err{..} = fs::create_dir("renders").await {
                }
        //Real Blender Call
        /*Command::new("blender")
                .args(&["-b", file_path, "-o","//renders/render####",
                 "-F", "PNG", "-x", "1", "-f", &format!("{}..{}", steve.frame_range.0, steve.frame_range.1)])
                 */
        Command::new("blender")
                .args(&[&format!("{}", steve.frame_range.0), &format!("{}", steve.frame_range.1)])
                .output()
                .expect("failed to execute process")
    };
    zip_send_renderd_files(&addr).await;
    let mut stream = TcpStream::connect(addr).await.expect("Couldn't connect to the server");
    let done = Packet {
        id: Role::Messenger,
        start_frame: 0,
        end_frame: 0,
        file: Vec::new(),
        file_name: String::from("Done"),
        file_size: 0
    };

    send_stuff(&done, &mut stream).await;
      
    if let Err{..} = fs::remove_dir_all("renders").await {
        println!("Some error occured when removing the renders directory, Continuing anyway");
    }
    if cfg!(target_os = "windows"){
        if let Err{..} = fs::remove_file("Blend_Tmp.blend").await {
            println!("Some error occured when removing the blend file, Continuing anyway");
        }
    }
    else {
        if let Err{..} = fs::remove_file("/tmp/Blend_Tmp.blend").await {
            println!("Some error occured when removing the blend file, Continuing anyway");
        }
    }
    

}


#[derive(Serialize, Debug)]
enum Role {
    Slave,
    _Customer,
    Messenger
}

#[derive(Serialize, Debug)]
struct Packet{
    id: Role,
    start_frame: u32,
    end_frame: u32,
    file: Vec<u8>,
    file_name: String,
    file_size: u64
}
#[derive(Deserialize, Debug)]
struct Job {
    file: Vec<u8>,
    frame_range: (u32, u32)
}
#[derive(Serialize, Debug)]
struct RenderdPng {
    file: Vec<u8>,
    file_name: String,
}


async fn write_blend(file: &mut Vec<u8>) -> &str {
    let output = if cfg!(target_os = "windows"){
        "Blend_Tmp.blend"
    } else {
        "/tmp/Blend_Tmp.blend"
    };
    let mut file = file.as_slice();
    let mut sys_file = fs::File::create(output).await.unwrap();
    loop{
        match sys_file.write_all(&mut file).await {
            Ok(_) => return output,

            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                println!("Would Block ?");
                continue;
            }

            Err(e) => {
                println!("Error: {}", e);
                panic!("Unknown Error");
            }

        }
    }
}


async fn handle_packet(stream: &mut TcpStream) -> Job {
    let mut buff:[u8;8] = [0;8];
    let mut new_buff:Vec<u8>;

    stream.read_exact(&mut buff).await.unwrap();
    //println!("{}", buff.capacity() as u64);
    //println!("{}", stream.limit());
        //Ok(_) => {
    match bincode::deserialize(&mut buff) {
            Ok(transmission) => {
                let transmission:usize = transmission;
                println!("Size of transmission is {}", &transmission);
                new_buff = vec![0; transmission];
                sizee = transmission;
            }
            Err(e) => {
                println!("The Error was {:?}", e);
                panic!();
            }
    }
    if let Ok(r) = stream.read_exact(&mut new_buff[..]).await{
        println!("Read {} Bytes", &r);
    };

    match bincode::deserialize(&new_buff) {
        Ok(mask) => {
            let mask: Job = mask;
            return mask;
        }
        Err(e) => {
            println!("The Error was {:?}", e);
            println!("The size of newbuff was {}", new_buff.len());
            panic!();

        }
    }
        //Err(e) => {
        //    println!("{}", e);
        //    panic!("Couldn't read from the stream");
        //}
}

async fn zip_send_renderd_files(addr: &str) {
    use std::path::Path;
    use flate2::Compression;
    use flate2::write::ZlibEncoder;
    let dir = std::fs::read_dir(Path::new("renders")).unwrap();
    
        for entry in dir {
            let mut stream = TcpStream::connect(addr).await.unwrap();
            let mut compressed_buff: Vec<u8> = Vec::with_capacity(100);
            let mut zip = ZlibEncoder::new(&mut compressed_buff, Compression::best());

            let mut buff: Vec<u8> = Vec::with_capacity(100);
            if let Ok(file) = entry {
                println!("{:?}", file.path());
                let entry = &file;
                let mut file = std::fs::File::open(file.path()).unwrap();
                file.read_to_end(&mut buff).unwrap();
                zip.write_all(&buff[..]).unwrap();
                zip.finish().unwrap();
                let prepared_write = Packet {
                    id: Role::Messenger,
                    file: compressed_buff,
                    file_name: entry.file_name().into_string().unwrap(),
                    file_size: file.metadata().unwrap().len(),
                    start_frame: 0,
                    end_frame: 0
                };
                send_stuff(&prepared_write, &mut stream).await;
                stream.shutdown().await.unwrap();
            };
        }
}


async fn send_stuff<T>(buff: &T, stream: &mut TcpStream)
where T: serde::Serialize {
    let file = bincode::serialize(&buff).unwrap();
    let size = file.len();
    println!("Size as deserialize {:?}",&bincode::serialize(&size).unwrap());

     stream.write_all(&bincode::serialize(&size).unwrap()).await.unwrap();
     println!("Sent {} bytes", &size);
     println!("{:?}", file);
     stream.write_all(&file).await.unwrap();
}
